function onSay(cid, words, param)
	local player = Player(cid)
	player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "ok")
	if not player:getGroup():getAccess() then
		return true
	end

	if player:getAccountType() < ACCOUNT_TYPE_GOD then
		return false
	end
	local params = param:split(',')
	local target = Player(params[1])
	if target then 
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, target:getStorageValue(Storage.ExplorerSociety.QuestLine))
	else
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, 'No target.')
	end

	return true
end

