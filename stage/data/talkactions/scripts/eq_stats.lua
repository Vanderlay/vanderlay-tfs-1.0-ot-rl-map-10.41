function onSay(cid, words, param)
	local player = Player(cid)
	local equipments = {
		{slot = 'weapon', 	item = player:getSlotItem(CONST_SLOT_LEFT)},
		{slot = 'shield', 	item = player:getSlotItem(CONST_SLOT_RIGHT)},
		{slot = 'head', 	item = player:getSlotItem(CONST_SLOT_HEAD)},
		{slot = 'armor', 	item = player:getSlotItem(CONST_SLOT_ARMOR)},
		{slot = 'legs', 	item = player:getSlotItem(CONST_SLOT_LEGS)},
		{slot = 'feet', 	item = player:getSlotItem(CONST_SLOT_FEET)}
	}
	local displayed = false
	for i = 1, #equipments do
		local equipment = equipments[i]
		local item = equipment.item
		if item and item:isItem() then
			local itemExp = item:getAttribute(ITEM_ATTRIBUTE_EXPERIENCE)
			if(itemExp > 0) then
				local itemLevel = getItemLevel(itemExp)
				local levelExp 	= getItemExpByLevel(itemLevel)
				local nextLevel = getItemExpByLevel(itemLevel + 1) - levelExp
				local currentExp = itemExp
				if(itemExp >= (itemLevelConfig.baseExp * itemLevelConfig.multiplier)) then
					currentExp = currentExp - levelExp
				else
					nextLevel = nextLevel + (itemLevelConfig.baseExp * itemLevelConfig.multiplier)
				end
				local itemName = item:getName()
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, itemName:gsub("^%l", string.upper) .. ": Current exp / Exp to level " .. itemLevel+1 .. " / Current stats bonus:")
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, currentExp .. " / " .. nextLevel .. " / +" .. math.floor(itemLevel / 10) .. ".")
				displayed = true
			end
		end
	end
	if not displayed then
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Your items have not gained any experience.")
	end
end