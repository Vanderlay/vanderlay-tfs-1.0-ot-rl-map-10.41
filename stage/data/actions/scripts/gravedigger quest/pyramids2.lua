function onUse(cid, item, fromPosition, itemEx, toPosition)
	if itemEx.actionid == 4663 or itemEx.actionid == 4664 or itemEx.actionid == 4665 or itemEx.actionid == 4666 or itemEx.actionid == 4667 then
		local player = Player(cid)
		if player:getStorageValue(Storage.GravediggerOfDrefia.Mission61) == 1 and player:getStorageValue(Storage.GravediggerOfDrefia.Mission62) < 1 then
			player:setStorageValue(Storage.GravediggerOfDrefia.Mission62, 1)
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, '<screeeech> <squeak> <squeaaaaal>')
		end
	end
	return true
end