function onUse(cid, item, fromPosition, itemEx, toPosition)
	local player = Player(cid)
	if not player then
		return false
	end
	
	if item.itemid ~= 21452 then
		return false
	end

	local creature = Tile(toPosition):getTopCreature()
	if not creature or creature:getName():lower() ~= 'gravedigger' then
		return false
	end

	player:addMount(39)
	player:say("You have successfully tamed a Gravedigger!", TALKTYPE_MONSTER_SAY)
	return true
end