local zone_teleports = {
	[22657]	= {
		[22657]  = {x = 32369, y = 32241, z = 7}
	}
}

function onStepIn(cid, item, position, fromPosition)
	print("dafqu")
	local section = zone_teleports[item.itemid]
	print("stepping")
	if not section then
		print("Not found")
		return false
	end
	local zoneTeleport = section[item.uid]
	if not zoneTeleport then
		return false
	end

	local player = Player(cid)
	player:teleportTo(zoneTeleport)

	return true
end