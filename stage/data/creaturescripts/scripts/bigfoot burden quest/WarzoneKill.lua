local bosses = {
	['deathstrike'] = {status = 2, storage = Storage.BigfootBurden.Warzone1Reward, restriction = Storage.BigfootBurden.Warzone1Restriction},
	['gnomevil'] = {status = 3, storage = Storage.BigfootBurden.Warzone2Reward, restriction = Storage.BigfootBurden.Warzone2Restriction},
	['abyssador'] = {status = 4, storage = Storage.BigfootBurden.Warzone3Reward, restriction = Storage.BigfootBurden.Warzone3Restriction},
}

function onKill(cid, target, lastHit)
	local targetMonster = Monster(target)
	if not targetMonster then
		return true
	end

	local bossConfig = bosses[targetMonster:getName():lower()]
	if not bossConfig then
		return true
	end

	for pid, _ in pairs(targetMonster:getDamageMap()) do
		local attackerPlayer = Player(pid)
		if attackerPlayer then
			if attackerPlayer:getStorageValue(Storage.BigfootBurden.WarzoneStatus) < bossConfig.status then
				attackerPlayer:setStorageValue(Storage.BigfootBurden.WarzoneStatus, bossConfig.status)
			end

			attackerPlayer:setStorageValue(bossConfig.storage, 1)
			attackerPlayer:setStorageValue(bossConfig.restriction, os.time() + (2 * 60 * 60))
		end
	end
end