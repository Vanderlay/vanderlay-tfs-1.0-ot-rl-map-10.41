function onLogout(cid)
	if mostDmgQueue[cid] then
		if mostDmgQueue[cid].queueType == 'party' then
			Player(cid):sendCancelMessage("You can't logout when queued as a party. To leave the queue, type !bg.")
			return false
		end
		mostDmgQueue[cid] = nil
	end

	local player = Player(cid)
	if player:isInsideArena() then
		local arena = getPlayerArena(cid)
		if arena then
			print ("Player logout: " .. player:getName())
			local playerPos = arena.players[cid].originalPosition
			player:teleportTo(Position(playerPos.x, playerPos.y, playerPos.z))
			arena.players[cid] = nil
			if player:isInsideArena() then
				player:switchInsideArena()
			end
			local party = Party(player)
			if arena.leader == cid  then
				for cid, name in pairs(arena.players) do
					local newLeader = Player(cid)
					if newLeader then
						arena.leader = cid
						if party:getLeader():getGuid() == player:getGuid() then
							party:setLeader(newLeader)
						end
						print("New leader: " .. newLeader:getName())
						break
					end
				end
			end
		end
	end
	return true
end