function onPrepareDeath(cid, killer)
	local arena = getPlayerArena(cid)
	local player = Player(cid)
	if arena then
		local player = Player(cid)
		if player then
			if arena.players[cid].team == 'redTeam' then
				basePos = arena.redPosition
			else
				basePos = arena.bluePosition
			end
			player:teleportTo(Position(basePos.x, basePos.y, basePos.z))
			player:addHealth(player:getMaxHealth())
			player:addMana(player:getMaxMana())
			return false
		end
	end
	return true
end