local config = {
	heal = true,
	save = true,
	effect = false
}

-- index == level required
-- index contains items and storage id
LEVEL_UP_STORAGE_ID_BASE = 88888
local levelUpItems = {
	[20] = {	-- level required
		storageId = 0,
		items = { 	-- items to give to the player
			[1]	= { -- voc id
				[2160] = 1		-- item id + quantity				
			},
			[2]	= {
				[2160] = 1
			},
			[3] = {
				[2160] = 1
			},
			[4] = {
				[2160] = 1
			},
			[5] = {
				[2160] = 1
			},
			[6] = {
				[2160] = 1
			},
			[7] = {
				[2160] = 1
			},
			[8] = {
				[2160] = 1
			},

		}
	},
	[35] = {	-- level required
		storageId = 1,
		items = { 	-- items to give to the player
			[1]	= { -- voc id
				[2160] = 1,		-- item id + quantity				
				[2187] = 1,
				[8900] = 1
			},
			[2]	= {
				[2160] = 1,
				[2183] = 1,
				[8900] = 1
			},
			[3] = {
				[2160] = 2,
				[2516] = 1
			},
			[4] = {
				[2160] = 1,
				[3962] = 1,
				[2436] = 1,
				[2407] = 1,
				[2516] = 1
			},
			[5] = {
				[2160] = 1,
				[2187] = 1,
				[8900] = 1
			},
			[6] = {
				[2160] = 1,
				[2183] = 1,
				[8900] = 1
			},
			[7] = {
				[2160] = 2,
				[2516] = 1
			},
			[8] = {
				[2160] = 1,
				[3962] = 1,
				[2436] = 1,
				[2407] = 1,
				[2516] = 1
			},

		}
	},
	[50] = {	-- level required
		storageId = 2,
		items = { 	-- items to give to the player
			[1]	= { -- voc id
				[2160] = 2,		-- item id + quantity
				[8902] = 1,
				[8922] = 1
			},
			[2]	= {
				[2160] = 2,
				[8902] = 1,
				[8910] = 1
			},
			[3] = {
				[2160] = 3,
				[2534] = 1
			},
			[4] = {
				[2160] = 3,
				[2534] = 1
			},
			[5] = {
				[2160] = 2,
				[8902] = 1,
				[8922] = 1
			},
			[6] = {
				[2160] = 2,
				[8902] = 1,
				[8910] = 1
			},
			[7] = {
				[2160] = 3,
				[2534] = 1
			},
			[8] = {
				[2160] = 3,
				[2534] = 1
			},

		}
	},
	[80] = {	-- level required
		storageId = 3,
		items = { 	-- items to give to the player
			[1]	= { -- voc id
				[2160] = 3		-- item id + quantity
			},
			[2]	= {
				[2160] = 3
			},
			[3] = {
				[2160] = 3
			},
			[4] = {
				[2160] = 3
			},
			[5] = {
				[2160] = 3
			},
			[6] = {
				[2160] = 3
			},
			[7] = {
				[2160] = 3
			},
			[8] = {
				[2160] = 3
			},

		}
	},
}

function onAdvance(cid, skill, oldLevel, newLevel)
	local player = Player(cid)
	if skill == 101 then
		for levelReq, data in pairs(levelUpItems) do
			if newLevel >= levelReq then
				if player:getStorageValue(LEVEL_UP_STORAGE_ID_BASE + data.storageId) ~= 1 then
					local vocId = player:getVocation():getId()
					for itemId, quantity in pairs(data.items[vocId]) do
						doPlayerAddItem(cid, itemId, quantity)
					end
					player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Congratulations - Here, have some free items!")
					player:setStorageValue(LEVEL_UP_STORAGE_ID_BASE + data.storageId, 1)
				end
			end
		end
	end
	if skill ~= 8 or newLevel <= oldLevel then
		return true
	end

	if config.effect then
		player:getPosition():sendMagicEffect(math.random(CONST_ME_FIREWORK_YELLOW, CONST_ME_FIREWORK_BLUE))
		player:say('LEVEL UP!', TALKTYPE_MONSTER_SAY)
	end

	if config.heal then
		player:addHealth(player:getMaxHealth())
	end
	if config.save then
		player:save()
	end
	return true
end
