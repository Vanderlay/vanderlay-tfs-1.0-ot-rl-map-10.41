local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)			npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)		npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)		npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()				npcHandler:onThink()					end

local function getTable()
	local list = {
		{id = 2316,		buy = 375,		sell = 0,		name='Animate dead rune'},
		{id = 2544,		buy = 3,		sell = 0,		name='Arrow'},
		{id = 2543,		buy = 4,		sell = 0,		name='Bolt'},
		{id = 2456,		buy = 400,		sell = 130,		name='Bow'},
		{id = 2455,		buy = 500,		sell = 160,		name='Crossbow'},
		{id = 18304,	buy = 20,		sell = 0,		name='Crystalline Arrow'},
		{id = 2310,		buy = 26,		sell = 0,		name='Desintegrate Rune'},
		{id = 18436,	buy = 12,		sell = 0,		name='Drill Bolt'},
		{id = 7850,		buy = 5,		sell = 0,		name='Earth Arrow'},
		{id = 2262,		buy = 162,		sell = 0,		name='Energy bomb rune'},
		{id = 2302, 	buy = 30,		sell = 0,		name='Fireball Rune'},
		{id = 7840, 	buy = 5,		sell = 0,		name='Flaming Arrow'},
		{id = 7838, 	buy = 5,		sell = 0,		name='Flash Arrow'},
		{id = 7591, 	buy = 190,		sell = 0,		name='Great Health Potion'},
		{id = 7590, 	buy = 120,		sell = 0,		name = 'Great Mana Potion'},
		{id = 8472, 	buy = 190,		sell = 0,		name = 'Great Spirit Potion'},	
		{id = 7618, 	buy = 45,		sell = 0,		name = 'Health Potion'},
		{id = 2295, 	buy = 16,		sell = 0,		name = 'Holy Missile Rune'},	
		{id = 2271, 	buy = 30,		sell = 0,		name = 'Icicle Rune'},
		{id = 2293, 	buy = 116,		sell = 0,		name = 'Magic Wall Rune'},	
		{id = 7620, 	buy = 50,		sell = 0,		name = 'Mana Potion'},
		{id = 7365, 	buy = 7,		sell = 0,		name = 'Onyx Arrow'},	
		{id = 2278, 	buy = 700,		sell = 0,		name = 'Paralyze Rune'},	
		{id = 7363, 	buy = 5,		sell = 0,		name = 'Piercing Bolt'},
		{id = 2547, 	buy = 7,		sell = 0,		name = 'Power Bolt'},
		{id = 18435, 	buy = 20,		sell = 0,		name = 'Prismatic Bolt'},	
		{id = 7378, 	buy = 15,		sell = 0,		name = 'Royal Spear'},
		{id = 7839, 	buy = 5,		sell = 0,		name = 'Shiver Arrow'},	
		{id = 2308, 	buy = 46,		sell = 0,		name = 'Soulfire Rune'},	
		{id = 2389, 	buy = 9,		sell = 3,		name = 'Spear'},
		{id = 2288, 	buy = 37,		sell = 0,		name = 'Stone Shower Rune'},	
		{id = 7588, 	buy = 100,		sell = 0,		name = 'Strong Health Potion'},
		{id = 7589, 	buy = 80,		sell = 0,		name = 'Strong Mana Potion'},
		{id = 15648, 	buy = 6,		sell = 0,		name = 'Tarsal Arrow'},
		{id = 2399, 	buy = 42,		sell = 0,		name = 'Throwing Star'},
		{id = 2315, 	buy = 37,		sell = 0,		name = 'Thunderstorm Rune'},	
		{id = 8473, 	buy = 310,		sell = 0,		name = 'Ultimate Health Potion'},	
		{id = 15649, 	buy = 6,		sell = 0,		name = 'Vortex Bolt'},
		{id = 2269, 	buy = 160,		sell = 0,		name = 'Wild Growth Rune'},
		{id = 22538, 	buy = 0,		sell = 500,		name = 'Bown of Terror Sweat'},
		{id = 22518, 	buy = 0,		sell = 1900,	name = 'Broken Visor'},
		{id = 22536, 	buy = 0,		sell = 450,		name = 'Dead Weight'},
		{id = 7636, 	buy = 0,		sell = 5,		name = 'Empty Potion Flask(small)'},
		{id = 7634, 	buy = 0,		sell = 5,		name = 'Empty Potion Flask(medium)'},
		{id = 7635, 	buy = 0,		sell = 5,		name = 'Empty Potion Flask(large)'},
		{id = 22533, 	buy = 0,		sell = 400,		name = 'Frazzle Skin'},
		{id = 22532, 	buy = 0,		sell = 700,		name = 'Frazzle Tongue'},
		{id = 22539, 	buy = 0,		sell = 650,		name = 'Goosebump Leather'},
		{id = 22540, 	buy = 0,		sell = 350,		name = 'Hemp rope'},
		{id = 22541, 	buy = 0,		sell = 480,		name = 'Pool of Chitinous Glue'},
		{id = 22517, 	buy = 0,		sell = 3000,	name = 'Sight of Surrender\'s Eye'},
		{id = 22534, 	buy = 0,		sell = 390,		name = 'Silencer Claws'},
		{id = 22535, 	buy = 0,		sell = 600,		name = 'Silencer Resonating Chamber'},
		{id = 22537, 	buy = 0,		sell = 900,		name = 'Trapped Bad Dream Monster'}
	}
	return list
end

local function creatureSayCallback(cid, type, msg)
	if not npcHandler:isFocused(cid) then
		return false
	end
	return true
end

local function onTradeRequest(cid)
	TradeRequest(cid, Npc(), getTable(), GreenDjinn, 4)
end

npcHandler:setMessage(MESSAGE_GREET, "Greetings, |PLAYERNAME|.")
npcHandler:setMessage(MESSAGE_FAREWELL, "Goodbye.")
npcHandler:setMessage(MESSAGE_WALKAWAY, "Goodbye.")

npcHandler:setCallback(CALLBACK_ONTRADEREQUEST, onTradeRequest)
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)

local focusModule = FocusModule:new()
focusModule:addGreetMessage('hi')
focusModule:addGreetMessage('hello')
npcHandler:addModule(focusModule)
