-- Piercing bolts effect
piercingBolts = Combat()
piercingBolts:setParameter(COMBAT_PARAM_BLOCKARMOR, 1)
piercingBolts:setParameter(COMBAT_PARAM_BLOCKSHIELD, 1)
piercingBolts:setParameter(COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
piercingBolts:setParameter(COMBAT_PARAM_EFFECT, CONST_ME_DRAWBLOOD)
piercingBolts:setParameter(COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_BOLT)
local area = createCombatArea({
	{0, 1, 1, 1, 0},
	{0, 0, 1, 0, 0},
	{0, 0, 3, 0, 0},
	{0, 0, 0, 0, 0}
})
piercingBolts:setArea(area)

-- Dual wield
dualWield = Combat()
dualWield:setParameter(COMBAT_PARAM_BLOCKARMOR, 1)
dualWield:setParameter(COMBAT_PARAM_BLOCKSHIELD, 1)
dualWield:setParameter(COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
dualWield:setParameter(COMBAT_PARAM_EFFECT, CONST_ME_DRAWBLOOD)
dualWield:setParameter(COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_NONE)
local dualWieldArea = createCombatArea({
	{0, 3, 0}
})
dualWield:setArea(dualWieldArea)